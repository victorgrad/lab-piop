package com.example.operation;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
//import static org.junit.Assert.assertThat;

public class OperationsTest {

    @Test
    public void testAdditionOperation() {
        BasicOperation op = new AdditionOperation();

        assertThat(op.compute(new BigDecimal(1), new BigDecimal(2)), is(new BigDecimal(3)));
        assertThat(op.compute(new BigDecimal(-30), new BigDecimal(5)), is(new BigDecimal(-25)));
        assertThat(op.compute(new BigDecimal("2.995"), new BigDecimal("0.005")), is(new BigDecimal("3.000")));
    }

    @Test
    public void testSubtractionOperation() {
        BasicOperation op = new SubtractionOperation();

        assertThat(op.compute(new BigDecimal(1), new BigDecimal(2)), is(new BigDecimal(-1)));
        assertThat(op.compute(new BigDecimal(-30), new BigDecimal(5)), is(new BigDecimal(-35)));
        assertThat(op.compute(new BigDecimal("2.995"), new BigDecimal("0.005")), is(new BigDecimal("2.990")));
    }

    @Test
    public void testMultiplicationOperation() {
        BasicOperation op = new MultiplicationOperation();

        assertThat(op.compute(new BigDecimal(1), new BigDecimal(2)), is(new BigDecimal(2)));
        assertThat(op.compute(new BigDecimal(-30), new BigDecimal(5)), is(new BigDecimal(-150)));
        assertThat(op.compute(new BigDecimal("2.995"), new BigDecimal("0.005")), is(new BigDecimal("0.014975")));
    }

    @Test
    public void testDivisionOperation() {
        BasicOperation op = new DivisionOperation();

        assertThat(op.compute(new BigDecimal(1), new BigDecimal(2)), is(new BigDecimal("0.5")));
        assertThat(op.compute(new BigDecimal(-30), new BigDecimal(5)), is(new BigDecimal(-6)));
        assertThat(op.compute(new BigDecimal("2.995"), new BigDecimal("0.005")), is(new BigDecimal("599")));
    }

    @Test
    public void testMaxOperation() {
        DifficultOperation op = new MaxOperation();

        assertThat(op.compute(List.of(new BigDecimal(1), new BigDecimal(2),new BigDecimal(3),new BigDecimal(4))), is(new BigDecimal(4)));
        assertThat(op.compute(List.of(new BigDecimal(-30), new BigDecimal(5))), is(new BigDecimal(5)));
        assertThat(op.compute(List.of(new BigDecimal("0.004"), new BigDecimal("0.005"))), is(new BigDecimal("0.005")));
    }

    @Test
    public void testMinOperation() {
        DifficultOperation op = new MinOperation();

        assertThat(op.compute(List.of(new BigDecimal(1), new BigDecimal(2),new BigDecimal(3),new BigDecimal(4))), is(new BigDecimal(1)));
        assertThat(op.compute(List.of(new BigDecimal(-30), new BigDecimal(5))), is(new BigDecimal(-30)));
        assertThat(op.compute(List.of(new BigDecimal("0.004"), new BigDecimal("0.005"))), is(new BigDecimal("0.004")));
    }

    @Test
    public void testSqrtOperation() {
        DifficultOperation op = new SqrtOperation();

        assertThat(op.compute(List.of(new BigDecimal(1))), is(new BigDecimal("1.0")));
        assertThat(op.compute(List.of(new BigDecimal(9))), is(new BigDecimal("3.0")));
        assertThat(op.compute(List.of(new BigDecimal("0.0004"))), is(new BigDecimal("0.02")));
    }
}
