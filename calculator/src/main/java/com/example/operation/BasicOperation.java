package com.example.operation;

import java.math.BigDecimal;

public interface BasicOperation {
    BigDecimal compute(BigDecimal first, BigDecimal second);
}
