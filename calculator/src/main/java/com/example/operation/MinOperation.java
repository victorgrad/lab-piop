package com.example.operation;

import java.math.BigDecimal;
import java.util.List;

public class MinOperation implements DifficultOperation{
    @Override
    public BigDecimal compute(List<BigDecimal> elems) {
        return elems.stream().min((o1, o2) -> o1.compareTo(o2)).get();
    }
}
