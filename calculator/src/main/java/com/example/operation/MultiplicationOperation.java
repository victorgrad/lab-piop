package com.example.operation;

import java.math.BigDecimal;

public class MultiplicationOperation implements BasicOperation {

    @Override
    public BigDecimal compute(BigDecimal first, BigDecimal second) {
        return first.multiply(second);
    }
}
