package com.example.operation;

import java.math.BigDecimal;

public class AdditionOperation implements BasicOperation {
    @Override
    public BigDecimal compute(BigDecimal first, BigDecimal second) {
        return first.add(second);
    }
}
