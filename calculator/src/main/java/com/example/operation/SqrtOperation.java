package com.example.operation;

import java.math.BigDecimal;
import java.util.List;

public class SqrtOperation implements DifficultOperation{
    @Override
    public BigDecimal compute(List<BigDecimal> elems) {
        return BigDecimal.valueOf(Math.sqrt(elems.get(0).doubleValue()));
    }
}
