package com.example.operation;

import java.math.BigDecimal;
import java.util.List;

public interface DifficultOperation{
    BigDecimal compute(List<BigDecimal> elems);
}
