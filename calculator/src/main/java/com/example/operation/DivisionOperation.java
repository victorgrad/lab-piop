package com.example.operation;

import java.math.BigDecimal;

public class DivisionOperation implements BasicOperation {
    @Override
    public BigDecimal compute(BigDecimal first, BigDecimal second) {
        return first.divide(second);
    }
}
