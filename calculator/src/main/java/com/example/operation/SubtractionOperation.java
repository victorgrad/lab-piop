package com.example.operation;

import java.math.BigDecimal;

public class SubtractionOperation implements BasicOperation {

    @Override
    public BigDecimal compute(BigDecimal first, BigDecimal second) {
        return first.subtract(second);
    }
}
