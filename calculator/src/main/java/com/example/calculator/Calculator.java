package com.example.calculator;

import com.example.operation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Calculator {
    private BigDecimal result;
    private static Calculator calculatorInstance;
    private static Scanner input;

    public static Calculator getInstance() {
        if (calculatorInstance == null) {
            calculatorInstance = new Calculator();
            input = new Scanner(System.in);
        }
        return calculatorInstance;
    }

    public void begin() {
        while (true) {
            if (result == null) {
                printResetMenu();
                String option = input.next();
                if(resetSwitchCase(option))
                    break;
            } else {
                printResultMenu();
                String option = input.next();
                if(resultSwitchCase(option))
                    break;
            }
        }
    }

    private void printResetMenu(){
        System.out.println("Select the operation:");
        System.out.println("1.Addition(+)");
        System.out.println("2.Subtraction(-)");
        System.out.println("3.Multiplication(*)");
        System.out.println("4.Division(/)");
        System.out.println("5.Min(min)");
        System.out.println("6.Max(max)");
        System.out.println("7.Sqrt(sqrt)");
        System.out.println("8.Exit(e)");
    }

    private void printResultMenu(){
        System.out.println("Result = " + result);
        System.out.println("Select the operation:");
        System.out.println("1.Addition(+)");
        System.out.println("2.Subtraction(-)");
        System.out.println("3.Multiplication(*)");
        System.out.println("4.Division(/)");
        System.out.println("5.CE(ce)");
        System.out.println("6.Sqrt(sqrt)");
        System.out.println("7.Exit(e)");
    }

    private boolean resetSwitchCase(String option){
        switch (option) {
            case "1":
            case "+": {
                doAddition();
                break;
            }
            case "2":
            case "-": {
                doSubtraction();
                break;
            }
            case "3":
            case "*": {
                doMultiplication();
                break;
            }
            case "4":
            case "/": {
                doDivision();
                break;
            }
            case "5":
            case "min": {
                doMin();
                break;
            }
            case "6":
            case "max": {
                doMax();
                break;
            }
            case "7":
            case "sqrt": {
                doSqrt();
                break;
            }
            case "8":
            case "e": {
                return true;
            }
            default: {
                System.out.println("Please select a valid operation!");
                break;
            }
        }
        return false;
    }

    private boolean resultSwitchCase(String option){
        switch (option) {
            case "1":
            case "+": {
                doAddition();
                break;
            }
            case "2":
            case "-": {
                doSubtraction();
                break;
            }
            case "3":
            case "*": {
                doMultiplication();
                break;
            }
            case "4":
            case "/": {
                doDivision();
                break;
            }
            case "5":
            case "ce": {
                doReset();
                break;
            }
            case "6":
            case "sqrt": {
                doSqrt();
                break;
            }
            case "7":
            case "e": {
                return true;
            }
            default: {
                System.out.println("Please select a valid operation!");
                break;
            }
        }
        return false;
    }

    private void doAddition() {
        List<BigDecimal> numbers = readTwoNumbers();
        BigDecimal first = numbers.get(0);
        BigDecimal second = numbers.get(1);

        BasicOperation op = new AdditionOperation();
        result = op.compute(first, second);
    }

    private void doSubtraction() {
        List<BigDecimal> numbers = readTwoNumbers();
        BigDecimal first = numbers.get(0);
        BigDecimal second = numbers.get(1);

        BasicOperation op = new SubtractionOperation();
        result = op.compute(first, second);
    }

    private void doMultiplication() {
        List<BigDecimal> numbers = readTwoNumbers();
        BigDecimal first = numbers.get(0);
        BigDecimal second = numbers.get(1);

        BasicOperation op = new MultiplicationOperation();
        result = op.compute(first, second);
    }

    private void doDivision() {
        List<BigDecimal> numbers = readTwoNumbers();
        BigDecimal first = numbers.get(0);
        BigDecimal second = numbers.get(1);

        BasicOperation op = new DivisionOperation();
        result = op.compute(first, second);
    }

    private void doMin() {
        List<BigDecimal> numbers = new ArrayList<>();
        String numbersString;
        System.out.println("Please enter the numbers separated by spaces: ");
        input.nextLine();
        numbersString = input.nextLine();
        List.of(numbersString.split(" ")).stream().forEach(string -> numbers.add(new BigDecimal(string)));
        DifficultOperation op = new MinOperation();
        result = op.compute(numbers);
    }

    private void doMax() {
        List<BigDecimal> numbers = new ArrayList<>();
        String numbersString;
        System.out.println("Please enter the numbers separated by spaces: ");
        input.nextLine();
        numbersString = input.nextLine();
        List.of(numbersString.split(" ")).stream().forEach(string -> numbers.add(new BigDecimal(string)));
        DifficultOperation op = new MaxOperation();
        result = op.compute(numbers);
    }

    private void doSqrt() {
        BigDecimal number;
        if (result != null) {
            number = result;
        } else {
            System.out.print("Please enter the number: ");
            number = new BigDecimal(input.next());
        }
        DifficultOperation op = new SqrtOperation();
        result = op.compute(List.of(number));
    }

    private void doReset() {
        result = null;
    }

    private List<BigDecimal> readTwoNumbers() {
        BigDecimal first;
        BigDecimal second;
        if (result != null) {
            first = result;
        } else {
            System.out.print("Input first number: ");
            first = new BigDecimal(input.next());
        }
        System.out.print("Input second number: ");
        second = new BigDecimal(input.next());
        return List.of(first, second);
    }
}
