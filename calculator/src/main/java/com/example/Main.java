package com.example;

import com.example.calculator.Calculator;

public class Main {
    public static void main(String[] args) {
        Calculator.getInstance().begin();
    }
}